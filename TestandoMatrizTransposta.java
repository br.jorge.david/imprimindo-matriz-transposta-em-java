package objetos;

import java.util.Scanner;

public class TestandoMatrizTransposta {
	static String[][] matriz;
	static int qtdDeLinhas = 0;
	static int qtdDeColunas = 0;
	static String inputLine = null;
	static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		System.out.println("Quantidade linhas da matriz: ");
		qtdDeLinhas = Integer.parseInt(scanner.nextLine());

		System.out.println("Quantidade colunas da matriz: ");
		qtdDeColunas = Integer.parseInt(scanner.nextLine());

		String matrizDigitada[][] = popularMatriz();
		if (matrizDigitada.length != 0) {
			imprimeMatriz(matrizDigitada);
			imprimeMatrizTransposta(matrizDigitada);
		} else {
			System.out.println("Matriz Nula");
		}
	}

	public static String[][] popularMatriz() {

		matriz = new String[qtdDeLinhas][qtdDeColunas];
		for (int x = 0; x < qtdDeLinhas; x++) {
			for (int y = 0; y < qtdDeColunas; y++) {
				System.out.print("Digite um valor: ");
				inputLine = scanner.nextLine();
				matriz[x][y] = inputLine;
			}
		}
		return matriz;
	}

	public static void imprimeMatriz(String matriz[][]) {
		System.out.println();
		System.out.println("Imprimindo Matriz Original");
		for (int i = 0; i < qtdDeLinhas; i++) {
			for (int j = 0; j < qtdDeColunas; j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static void imprimeMatrizTransposta(String matriz[][]) {
		System.out.println();
		System.out.println("Imprimindo Matriz Transposta");
		for (int i = 0; i < qtdDeColunas; i++) {
			for (int j = 0; j < qtdDeLinhas; j++) {
				System.out.print(matriz[j][i] + " ");
			}
			System.out.println();
		}
	}

}
